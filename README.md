# 42-common-core-projects

## About

These are my solutions for the first projects of school 42. \
They are meant to teach the fundamentals of computer science with C and C++ languages.

### We learn different programming ways : 
* imperative
* parallel
* object-oriented

### Some projects ask us to recreate basic functions :
* libft : string manipulation 
* printf : to print text and variables
* get_next_line : read from a buffer...

### Some other are more complex programms :
* so_long : small 2D video game 
* push_swap : sorting algorithms
* minishell : reproduction of a shell similar to bash

### And some help us understand different concepts around the programmation itself, such as virtualization and networks.

***

## How to

```
git clone https://gitlab.com/Ralphiki/42-common-core-projects.git
cd 42-common-core-projects/project_name
make
./execution_file
```

***

# Links

my original repository : https://github.com/Aspinaut/42
